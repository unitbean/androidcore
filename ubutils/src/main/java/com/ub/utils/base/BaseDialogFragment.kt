package com.ub.utils.base

import com.arellomobile.mvp.MvpAppCompatDialogFragment


abstract class BaseDialogFragment : MvpAppCompatDialogFragment()